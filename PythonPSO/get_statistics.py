import cv2
import numpy as np
import matplotlib.pyplot as plt

max_img_width = 1280
ref_shape_stats = list()

for i in range(309):
    n_img = i

    img_filename = '../tennis_ball_datasets/'+str(n_img)+'.jpg'
    txt_filename = '../tennis_ball_datasets/'+str(n_img)+'.txt'
    out_ref_name = '../tennis_ball_datasets/'+str(n_img)+'_ref.jpg'

    image = cv2.imread(img_filename)

    scale_factor = 1.0
    if image.shape[1] > max_img_width:
        scale_factor = max_img_width / image.shape[1]
        image = cv2.resize(image, None, fx=scale_factor, fy=scale_factor)

    with open(txt_filename) as f:
        array = []
        for line in f:  # read rest of lines
            array.append([float(x) for x in line.split()])

    if len(array) != 1:
        continue
    else:
        if len(array[0]) != 5:
            continue

    img_width = image.shape[1]
    img_height = image.shape[0]
    x = int(array[0][1] * img_width)
    y = int(array[0][2] * img_height)
    w = int((array[0][3] * img_width)/2)
    h = int((array[0][4] * img_height)/2)

    # make reference shape to be square
    if w > h:
        h = w
    else:
        w = h

    reference = image[y-h+1:y+h+1, x-w+1:x+w+1]
    ref_width = reference.shape[1]
    ref_height = reference.shape[0]

    print("Adding stats for i = ", i)
    ref_shape_stats.append(w)
    cv2.imwrite(out_ref_name, reference)


print(np.mean(ref_shape_stats))
n_bins = np.max(ref_shape_stats) - np.min(ref_shape_stats) + 1

plt.figure()
plt.hist(ref_shape_stats, n_bins)
plt.show()