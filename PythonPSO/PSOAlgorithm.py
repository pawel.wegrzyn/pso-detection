import cv2
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import mean_squared_error
from skimage.measure import compare_ssim as ssim
import copy


class PSOAlgorithm:

    def __init__(self, image, reference, particles=30, c_local=1, c_global=1, inertia=0.7, max_it=100, explode_cnt=20,
                 max_velocity=20, cost_type="MSE_PART", max_scale_velocity=0.3, scale_min_max=(0.4, 1.6), use_scale=True):
        # update image and reference
        self.image_ = image
        self.reference_ = reference

        # update PSO algorithm parameters
        self.particles_ = particles
        self.c_local_ = c_local
        self.c_global_ = c_global
        self.inertia_ = inertia
        self.max_it_ = max_it
        self.cost_type_ = cost_type
        self.explode_cnt_ = explode_cnt
        self.max_velocity_ = max_velocity
        self.max_scale_velocity = max_scale_velocity
        self.scale_min_max = scale_min_max
        self.use_scale_ = use_scale

        # create structures and init PSO (two states - x,y positions. In future: add scale
        self.state_vector_ = np.zeros((self.particles_, 3), np.float)
        self.velocities_ = np.zeros((self.particles_, 3), np.float)
        self.local_best_cost_ = np.full(self.particles_, np.float('Inf'), np.float)
        self.current_costs_ = np.full(self.particles_, np.float('Inf'), np.float)
        self.local_best_state_vector_ = np.zeros((self.particles_, 3), np.float)
        self.global_best_cost_ = np.float('Inf')
        self.global_best_state_vector_ = np.zeros(3, np.float)
        self.global_best_buffer_ = list()
        self.global_best_watchdog_cnt_ = 0
        self.state_vector_buffer_ = list()
        self.best_cost_buffer_ = list()
        # self.scale_buffer = list()

    def run(self):
        self.init_pso()
        for i in range(self.max_it_):
            self.update_particles_cost()
            self.update_particles_best()
            if self.global_best_watchdog_cnt_ == self.explode_cnt_:
                self.explode()
            self.update_particle_velocities()
            self.update_particle_positions()

            self.global_best_buffer_.append(copy.deepcopy(self.global_best_state_vector_))
            self.best_cost_buffer_.append(copy.deepcopy(self.global_best_cost_))
            self.state_vector_buffer_.append(copy.deepcopy(self.state_vector_))

    def get_result(self):
        return self.global_best_state_vector_, self.global_best_cost_

    def init_pso(self):

        # reset parameters and buffers
        self.local_best_cost_ = np.full(self.particles_, np.float('Inf'), np.float)
        self.current_costs_ = np.full(self.particles_, np.float('Inf'), np.float)
        self.local_best_state_vector_ = np.zeros((self.particles_, 3), np.float)
        self.global_best_cost_ = np.float('Inf')
        self.global_best_state_vector_ = np.zeros(3, np.float)
        self.global_best_watchdog_cnt_ = 0
        self.state_vector_buffer_ = np.zeros((self.particles_, 3, self.max_it_))
        self.state_vector_buffer_ = list()
        self.global_best_buffer_ = list()
        self.best_cost_buffer_ = list()

        # generate random particles positions and velocities

        # TODO: initial scale value in linear space case
        linear_space = False
        if linear_space:
            width = self.image_.shape[1]
            height = self.image_.shape[0]
            w_size = int(width/8)
            h_size = int(height/8)
            number_of_blocks = w_size * h_size
            particles_per_block = int(self.particles_ / number_of_blocks)
            rest_particles = self.particles_ - number_of_blocks*particles_per_block

            for i in range(number_of_blocks):
                w_min = (number_of_blocks % 8) * w_size
                w_max = (number_of_blocks % 8) * w_size + w_size
                h_min = int(number_of_blocks / 8) * h_size
                h_max = int(number_of_blocks / 8) * h_size + h_size
                self.state_vector_[i*particles_per_block:(i+1)*particles_per_block, 0] = np.random.uniform(h_min, h_max, particles_per_block)  # generate y position randomly
                self.state_vector_[i*particles_per_block:(i+1)*particles_per_block, 1] = np.random.uniform(w_min, w_max, particles_per_block)  # generate x position randomly

            self.state_vector_[self.particles_-rest_particles:self.particles_, 0] = np.random.uniform(0, self.image_.shape[0], self.particles_)  # generate y position randomly
            self.state_vector_[self.particles_-rest_particles:self.particles_, 1] = np.random.uniform(0, self.image_.shape[1], self.particles_)  # generate x position randomly

        else:
            self.state_vector_[:, 0] = np.random.uniform(0, self.image_.shape[0], self.particles_)  # generate y position randomly
            self.state_vector_[:, 1] = np.random.uniform(0, self.image_.shape[1], self.particles_)  # generate x position randomly
            self.state_vector_[:, 2] = np.random.uniform(self.scale_min_max[0], self.scale_min_max[1])  # generate scale randomly

        self.velocities_[:, 0] = np.random.uniform(0, 5.0, self.particles_)
        self.velocities_[:, 1] = np.random.uniform(0, 5.0, self.particles_)
        self.velocities_[:, 2] = np.random.uniform(0, 0.1, self.particles_)

        self.state_vector_buffer_.append(copy.deepcopy(self.state_vector_))

    def explode(self):
        # generate random particles positions and velocities
        self.state_vector_[:, 0] = np.random.uniform(0, self.image_.shape[0], self.particles_)  # generate y position randomly
        self.state_vector_[:, 1] = np.random.uniform(0, self.image_.shape[1], self.particles_)  # generate x position randomly
        self.state_vector_[:, 2] = np.random.uniform(self.scale_min_max[0], self.scale_min_max[1])  # generate scale randomly

        self.velocities_[:, 0] = np.random.uniform(0, 5.0, self.particles_)
        self.velocities_[:, 1] = np.random.uniform(0, 5.0, self.particles_)
        self.velocities_[:, 2] = np.random.uniform(0, 0.1, self.particles_)
        print("Explosion!")

    def update_particles_cost(self):
        for i in range(self.particles_):
            self.current_costs_[i] = self.evaluate_cost(
                int(self.state_vector_[i, 0]), int(self.state_vector_[i, 1]), self.state_vector_[i, 2])

    def update_particles_best(self):
        global_best_updated = False
        for i in range(self.particles_):
            if self.current_costs_[i] < self.local_best_cost_[i]:
                self.local_best_cost_[i] = self.current_costs_[i]  # update minimal local cost
                self.local_best_state_vector_[i, :] = copy.deepcopy(self.state_vector_[i, :])
            if self.current_costs_[i] < self.global_best_cost_:
                if np.abs(self.global_best_state_vector_[0] - self.state_vector_[i, 0]) <= 1 and np.abs(
                        self.global_best_state_vector_[1] - self.state_vector_[i, 1]) <= 1:
                    global_best_updated = False
                else:
                    self.global_best_cost_ = copy.deepcopy(self.current_costs_[i])  # update minimal global cost
                    self.global_best_state_vector_ = copy.deepcopy(self.state_vector_[i, :])
                    global_best_updated = True

        if global_best_updated:
            self.global_best_watchdog_cnt_ = 0
        else:
            self.global_best_watchdog_cnt_ += 1

    def update_particle_velocities(self):
        for i in range(self.particles_):
            alfa_1 = np.random.uniform(0.0, 1.0, 1)
            alfa_2 = np.random.uniform(0.0, 1.0, 1)
            for idx in range(3):
                self.velocities_[i, idx] = self.inertia_*self.velocities_[i, idx] + \
                                        alfa_1*self.c_local_*(self.local_best_state_vector_[i, idx] - self.state_vector_[i, idx]) + \
                                        alfa_2*self.c_global_*(self.global_best_state_vector_[idx] - self.state_vector_[i, idx])

                if idx < 2:
                    if self.velocities_[i, idx] > self.max_velocity_:
                        self.velocities_[i, idx] = self.max_velocity_
                    elif self.velocities_[i, idx] < -self.max_velocity_:
                        self.velocities_[i, idx] = -self.max_velocity_
                else:
                    if self.velocities_[i, idx] > self.max_scale_velocity:
                        self.velocities_[i, idx] = self.max_scale_velocity
                    elif self.velocities_[i, idx] < -self.max_scale_velocity:
                        self.velocities_[i, idx] = -self.max_scale_velocity

    def update_particle_positions(self):
        for i in range(self.particles_):
            for idx in range(2):
                self.state_vector_[i, idx] = self.state_vector_[i, idx] + self.velocities_[i, idx]

                if self.state_vector_[i, idx] > self.image_.shape[idx]:
                    self.state_vector_[i, idx] = np.random.uniform(0, self.image_.shape[idx], 1)

            # scale update
            self.state_vector_[i, 2] = self.state_vector_[i, 2] + self.velocities_[i, 2]
            if self.state_vector_[i, 2] < self.scale_min_max[0] or \
                    self.state_vector_[i, 2] > self.scale_min_max[1]:
                self.state_vector_[i, 2] = np.random.uniform(self.scale_min_max[0],
                                                             self.scale_min_max[1])

    def evaluate_cost(self, idy, idx, scale):

        if not self.use_scale_:
            scale = 1
        dim = (int(self.reference_.shape[0]*scale), int(self.reference_.shape[1]*scale))
        scaled_reference = cv2.resize(self.reference_, dim[0:2])
        src_shape = self.image_.shape
        if idx < 0 or idy < 0:
            # self.reference_ image beyond the border
            return np.float('Inf')
        elif scaled_reference.shape[0] + idy > src_shape[0] or scaled_reference.shape[1] + idx > src_shape[1]:
            # scaled_reference image beyond the border
            return np.float('Inf')
        else:
            src_image_ref = self.image_[idy:idy + scaled_reference.shape[0], idx:idx + scaled_reference.shape[1]]
            if self.cost_type_ == "MSE_PART":
                # calculate cost as a MSE
                ref_vect = np.concatenate([scaled_reference[:, :, 1].ravel(),
                                           scaled_reference[:, :, 2].ravel()])
                src_ref_vect = np.concatenate([src_image_ref[:, :, 1].ravel(),
                                               src_image_ref[:, :, 2].ravel()])
                mse = mean_squared_error(ref_vect, src_ref_vect)
                return np.sqrt(mse)

            elif self.cost_type_ == "MSE_FULL":
                # calculate cost as a MSE
                ref_vect = np.concatenate([scaled_reference[:, :, 0].ravel(),
                                           scaled_reference[:, :, 1].ravel(),
                                           scaled_reference[:, :, 2].ravel()])
                src_ref_vect = np.concatenate([src_image_ref[:, :, 0].ravel(),
                                               src_image_ref[:, :, 1].ravel(),
                                               src_image_ref[:, :, 2].ravel()])
                mse = mean_squared_error(ref_vect, src_ref_vect)
                return np.sqrt(mse)

            elif self.cost_type_ == "SSIM":
                src_image_ref_gray = cv2.cvtColor(src_image_ref, cv2.COLOR_BGR2GRAY)
                ref_gray = cv2.cvtColor(scaled_reference, cv2.COLOR_BGR2GRAY)
                return 1.0 - ssim(src_image_ref_gray, ref_gray)

    def get_costmap(self):
        costmap = np.zeros(self.image_.shape, np.float)
        for idy_val in range(costmap.shape[0]):
            for idx_val in range(costmap.shape[1]):
                costmap[idy_val, idx_val] = self.evaluate_cost(idy_val, idx_val, 1)

        costmap[costmap == np.float('Inf')] = -1.0
        max_cost = np.max(costmap)
        costmap[costmap == -1] = max_cost
        min_cost = np.min(costmap)
        costmap = np.interp(costmap, (min_cost, max_cost), (0, 255))
        return costmap
