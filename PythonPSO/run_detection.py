import cv2
import matplotlib.pyplot as plt
import numpy as np
from PSOAlgorithm import PSOAlgorithm
import time
import copy

n_img = 260
ref_numbers = [250, 260, 270, 280, 290, 300]
# n_ref = 289
max_img_width = 1280
read_ref_direct = False
color_space = cv2.COLOR_BGR2YCR_CB
# color_space = cv2.COLOR_BGR2RGB
# color_space = cv2.COLOR_BGR2HSV

img_filename = '../tennis_ball_datasets/'+str(n_img)+'.jpg'

ref_txt_filenames = []
ref_filenames = []
for n_ref in ref_numbers:
    ref_filenames.append('../tennis_ball_datasets/'+str(n_ref)+'.jpg')
    ref_txt_filenames.append('../tennis_ball_datasets/'+str(n_ref)+'.txt')

references = []
for (ref_filename, ref_txt_filename) in zip(ref_filenames, ref_txt_filenames):
    image = cv2.cvtColor(cv2.imread(img_filename), color_space)
    image_rgb = cv2.cvtColor(cv2.imread(img_filename), cv2.COLOR_BGR2RGB)
    ref_image = cv2.cvtColor(cv2.imread(ref_filename), color_space)
    scale_factor = 1.0
    if image.shape[1] > max_img_width:
        scale_factor = max_img_width / image.shape[1]
        ref_image = cv2.resize(ref_image, None, fx=scale_factor, fy=scale_factor)
        image_rgb = cv2.resize(image_rgb, None, fx=scale_factor, fy=scale_factor)
        image = cv2.resize(image, None, fx=scale_factor, fy=scale_factor)

    with open(ref_txt_filename) as f:
        array = []
        for line in f:  # read rest of lines
            array.append([float(x) for x in line.split()])

    img_width = ref_image.shape[1]
    img_height = ref_image.shape[0]
    x = int(array[0][1] * img_width)
    y = int(array[0][2] * img_height)
    w = int((array[0][3] * img_width)/2)
    h = int((array[0][4] * img_height)/2)

    reference = ref_image[y-h+1:y+h+1, x-w+1:x+w+1]
    references.append(reference)
    # plt.figure()
    # plt.imshow(reference)
    # plt.title('Reference')
    # plt.show()


start = time.time()


scores = {}
count = 0
best_id = 0
for reference in references:
    PSO = PSOAlgorithm(image, reference, particles=100, c_local=1.2, c_global=0.8, inertia=0.7, explode_cnt=10,
                       max_it=60, max_velocity=100, cost_type="MSE_PART", max_scale_velocity=0.3, scale_min_max=(0.4, 1.6),
                       use_scale=True)
    PSO.run()

    stop = time.time()

    best_position, best_cost = PSO.get_result()
    scores[count] = [copy.deepcopy(best_cost), copy.deepcopy(best_position)]
    if best_cost < scores[best_id][0]:
        best_id = copy.deepcopy(count)
        print("new best id = {}".format(best_id))
    print("Best cost: ", best_cost)
    print("PSO time:  ", str(stop-start))
    count += 1


    # get costmap
    # costmap = PSO.get_costmap()
    # plt.figure()
    # plt.imshow(costmap.astype('uint8'), cmap='gray')
    # plt.title('Costmap')
    # plt.show()


    # plt.ion()
    # plt.figure(figsize=[16, 9])
    #
    # for it in range(len(PSO.state_vector_buffer_)-1):
    #     current_image = copy.deepcopy(image_rgb)
    #     for part_nr in range(len(PSO.state_vector_buffer_[0])):
    #         point_src = tuple(PSO.state_vector_buffer_[it][part_nr, 0:2].astype(int))
    #         point_src = tuple(reversed(point_src))
    #         point_dst = tuple(PSO.state_vector_buffer_[it+1][part_nr, 0:2].astype(int))
    #         point_dst = tuple(reversed(point_dst))
    #         cv2.arrowedLine(current_image, point_src, point_dst, (255, 0, 0), 2)
    #
    #     best_x = int(PSO.global_best_buffer_[it][1])
    #     best_y = int(PSO.global_best_buffer_[it][0])
    #     current_image[best_y-4+1:best_y+4, best_x-4+1:best_x+4] = [0, 255, 0]
    #
    #     plt.imshow(current_image)
    #     plt.title("Best cost = " + str(PSO.best_cost_buffer_[it]))
    #     plt.draw()
    #     plt.pause(0.3)
    #     plt.clf()
    #
    # plt.ioff()


idy_min_cost = int(scores[best_id][1][1])
idx_min_cost = int(scores[best_id][1][0])

print("best cost from all references = {}".format(scores[best_id][0]))
cv2.rectangle(image_rgb, (idy_min_cost, idx_min_cost), (idy_min_cost + references[best_id].shape[1],
                                                        idx_min_cost + references[best_id].shape[0]), (0, 255, 0), 3)

plt.figure()
plt.imshow(image_rgb)
plt.title('Image')
plt.show()
